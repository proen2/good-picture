<?php
  session_start();
  $sid=session_id();

  if($_SESSION[$sid]["sid"]==$sid){
  }else {
    header("Location: login.php");
  }

  $name = isset($_POST['name']) ? $_POST['name'] : '';
  $upfile = isset($_FILES["upfile"]["tmp_name"]) ? $_FILES["upfile"]["tmp_name"] : '';

  if($name && $upfile){
    include("php/connect.php");
    try{
  	  $imgdat = file_get_contents($upfile);
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      $mime_type = finfo_buffer($finfo, $imgdat);
      finfo_close($finfo);
      $extension_array = array(
                  'gif' => 'image/gif',
                  'jpg' => 'image/jpeg',
                  'png' => 'image/png'
                  );
      if($img_extension = array_search($mime_type, $extension_array,true)){
        $imgdat = @mysql_real_escape_string($imgdat);
        $conn->exec("INSERT INTO goodimage(`id`, `name`, `binary`, `good`, `bad`) VALUES (NULL, '{$name}', '{$imgdat}' ,'{0}' , '{0}')");
        $newId = $conn->lastInsertId();
      }
    }catch(SQLException $e){
      die($e->getMessage());
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <form action="upload.php" method="POST" class="form-container" enctype='multipart/form-data'>
      <div class="form-title">
        <h2>アップロード</h2>
      </div>
      <div class="form-title">画像の名前を入力してください</div>
      <input class="form-field" type="text" name="name" />
      <br>
      <div class="form-title">アップロードしたい画像ファイルを選択してください</div>
      <input type='hidden' name='MAX_FILE_SIZE' value='1000000' />
      <input type="file" name="upfile"/>
      <br>
      <div class="submit-container">
        <input class="submit-button" type="submit" value="Upload">
      </div>
    </form>
    <?php
      if(isset($newId)){
        echo("<div class='msg'>画像を追加しました。</div>");
      }
    ?>
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
</body>

</html>
