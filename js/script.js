$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
$(document).ready(function() {
  $('#pic_l').fadeIn(1500);
  $('#pic_r').fadeIn(3000);
});
$(function() {
  $('#rank').click(function() {
    $(this)
      .animate({
        'marginLeft': '10px'
      }, 20)
      .animate({
        'marginLeft': '-8px'
      }, 20)
      .animate({
        'marginLeft': '6px'
      }, 20)
      .animate({
        'marginLeft': '-4px'
      }, 20)
      .animate({
        'marginLeft': '2px'
      }, 20)
      .animate({
        'marginLeft': '-0px'
      }, 20);
  });
});
