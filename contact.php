<?php
  session_start();
  $sid=session_id();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <div class="main_contents">
      <form method="POST" id="contact_form" class="form-container" action="send.php">
        <div class="form-title">
          <h2>お問い合わせフォーム</h2>
        </div>
        <div class="form-title">名前</div>
        <input class="form-field" type="text" name="firstname" required>
        <br>
        <div class="form-title">Email</div>
        <input class="form-field" type="text" name="email" required>
        <br>
        <div class="form-title">内容</div>
        <textarea id="content_area" class="form-field" name="content" rows="8" cols="25" form="contact_form"></textarea>
        <br>
        <div class="submit-container">
          <input class="submit-button" type="submit" value="送信">
        </div>
      </form>
    </div>
    <!--wrapper-->
    <footer>
      <div class="footer_content clearfix">
        <ul class="footer_list clearfix">
          <li>
            <a href="index.php">HOME</a>
          </li>
          <li>|</li>
          <li>
            <a href="create.php">ユーザー登録</a>
          </li>
          <li>|</li>
          <li>
            <a href="contact.php">お問い合わせ</a>
          </li>
        </ul>
      </div>
      <!--footer_content-->
    </footer>
</body>

</html>
