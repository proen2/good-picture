<?php
  session_start();
  $sid=session_id();

  function createRand () { // テーブル数の数を最大値にした乱数を生成する
    include("php/connect.php");
    try{
      $count = $conn -> query("SELECT COUNT(*) FROM goodimage");
      $c = $count -> fetch();
      $co = $c[0]; // テーブル数
      $rand = mt_rand(1,$co); // 乱数生成
    }catch(SQLException $e){
      die($e->getMessage());
      $rand = null;
    }
    return $rand;
  }

  include("php/connect.php");
  try{
    $result = $conn->query("SELECT * FROM goodimage order by good DESC limit 5");
    $r = $result -> fetchall();
  }catch(SQLException $e){
    die($e->getMessage());
  }

  $pic1 = createRand();
  $pic2 = createRand();
  do { // $pic1と$pic2が同じ数字にならないようにループで処理
    $pic2 = createRand();
  } while ($pic1 == $pic2);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/animations.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
  <script src="js/jqfloat.min.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){ //ログインしていない場合は、ログインボタンを表示
                                      //ログインしているときは、ユーザーページへのリンクを表示
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力" required>
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <div class="main_contents">
      <section id="sec_01">
        <img id="which_pic" class="pulse" src="img/which.png" alt="which is good" />
        <div class="pic_frame">
          <a href="page.php?id=<?php echo $pic1 ?>">
           <img id="pic_l" src="php/readImage.php?id=<?php echo $pic1 ?>" alt="pic1" />
          </a>
          <img id="vs_pic" src="img/vs.png" alt="vs" />
           <a href="page.php?id=<?php echo $pic2 ?>">
            <img id="pic_r" src="php/readImage.php?id=<?php echo $pic2 ?>" alt="pic2" />
           </a>
        </div>
        <!-- .pic_frame -->
        <ul class="btn_list">
          <li>
            <form method="POST" action="page.php?id=<?php echo $pic1 ?>">
              <input type="submit" class="good_btn" value="Good!!" name="good_btn">
            </form>
          </li>
          <li>
            <form method="POST" action="page.php?id=<?php echo $pic2 ?>">
              <input type="submit" class="good_btn" value="Good!!" name="good_btn">
            </form>
          </li>
        </ul>
        <!-- .btn_list -->
      </section>
      <!-- #sec_01 -->
      <section id="sec_02">
        <div class="total_rank">
          <p id="rank">Ranking</p>
          <div class="slide_box">
            <div class="flexslider">
              <ul class="slides">
                <li>
                  <a href="page.php?id=<?php echo $r[0]['id'] ?>">
                  <img src="php/readImage.php?id=<?php echo $r[0]['id'] ?>" alt="pic" />
                  </a>
                  <p class="flex-caption">
                    <span>第1位</span>
                    <br>
                    <?php
                      echo $r[0]["name"]."<br>".$r[0]["good"]."票";
                    ?>
                  </p>
                </li>
                <li>
                  <a href="page.php?id=<?php echo $r[1]['id'] ?>">
                  <img src="php/readImage.php?id=<?php echo $r[1]['id'] ?>" alt="pic" />
                  </a>
                  <p class="flex-caption">
                    <span>第2位</span>
                    <br>
                    <?php
                      echo $r[1]["name"]."<br>".$r[1]["good"]."票";
                    ?>
                  </p>
                </li>
                <li>
                  <a href="page.php?id=<?php echo $r[2]['id'] ?>">
                  <img src="php/readImage.php?id=<?php echo $r[2]['id'] ?>" alt="pic" />
                  </a>
                  <p class="flex-caption">
                    <span>第3位</span>
                    <br>
                    <?php
                      echo $r[2]["name"]."<br>".$r[2]["good"]."票";
                    ?>
                  </p>
                </li>
                <li>
                  <a href="page.php?id=<?php echo $r[3]['id'] ?>">
                  <img src="php/readImage.php?id=<?php echo $r[3]['id'] ?>" alt="pic" />
                  </a>
                  <p class="flex-caption">
                    <span>第4位</span>
                    <br>
                    <?php
                      echo $r[3]["name"]."<br>".$r[3]["good"]."票";
                    ?>
                  </p>
                </li>
                <li>
                  <a href="page.php?id=<?php echo $r[4]['id'] ?>">
                  <img src="php/readImage.php?id=<?php echo $r[4]['id'] ?>" alt="pic" />
                  </a>
                  <p class="flex-caption">
                    <span>第5位</span>
                    <br>
                    <?php
                      echo $r[4]["name"]."<br>".$r[4]["good"]."票";
                    ?>
                  </p>
                </li>
              </ul>
              <!-- .slides -->
            </div>
            <!-- .flexslider -->
          </div>
          <!-- .slide_box -->
        </div>
        <!-- total_rank -->
      </section>
      <!-- #sec_02 -->
    </div>
    <!-- .main_contents -->
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
</body>

</html>
