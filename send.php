<?php
  $firstname = $_POST['firstname'];
  $email = $_POST['email'];
  $contents = $_POST['content'];
  $contents = nl2br($contents);

  $data = "\r\n";
  $data = $data."投稿者:".$firstname."\r\n";
  $data = $data."e-mail:".$email."\r\n";
  $data = $data."内容:".$contents."\r\n";

  $fp = fopen('txt/log.txt', 'ab');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <?php
      // お問い合わせの内容はtxt/log.txtに書き込む
      if ($fp){
        if (flock($fp, LOCK_EX)){
          if (fwrite($fp,  $data) === FALSE){
            print('ファイル書き込みに失敗しました');
          }else {
            echo "お問い合わせありがとうございました<br>";
            echo '<a href="index.php">トップページへ戻る</a>';
          }
          flock($fp, LOCK_UN);
        }else{
          print('ファイルロックに失敗しました');
        }
      }
      fclose($fp);
    ?>
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
</body>

</html>
