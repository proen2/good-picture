<?php
  session_start();
  $sid=session_id();

	$userID = isset($_POST['userID']) ? $_POST['userID'] : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';
	$message = '';
	$message2 = '';

	if($userID && $password){
		include("php/connect.php");
		try{
		  $result = $conn -> query(
		    "SELECT * FROM GP_users WHERE "
		    ."userID = '{$userID}' AND password = '{$password}'"
		  );
		  $r = $result -> fetch();
		  if($r){ // ログインができた場合はようこそメッセージとユーザーページへのリンクを表示
		    $message = "ようこそ、{$r['name']}さん！<br>";
	      $message2 = '<a class="users_link" href="users.php?user='.$r['userID'].'">ユーザーページヘ</a>';
				$_SESSION[$sid]["sid"] = $sid;
				$_SESSION["userID"] = $r['userID'];
				$_SESSION["name"] = $r['name'];
		  }else{
		    $message = "ユーザ名もしくはパスワードが違っています。";
		  }
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <div class="main_contents">
      <form method="POST" action="login.php" class="form-container">
        <div class="form-title">
          <h2>ログイン</h2>
        </div>
        <div class="form-title">userID</div>
        <input class="form-field" type="text" name="userID">
        <br>
        <div class="form-title">Password</div>
        <input class="form-field" type="password" name="password">
        <br>
        <div class="submit-container">
          <input class="submit-button" type="submit" value="送信">
        </div>
      </form>
      <hr>
      <div class="msg">
        <?php
          echo $message;
	        echo $message2;
        ?>
      </div>
      <!-- .error_msg -->
    </div>
    <!-- main_contennts -->
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
</body>

</html>
