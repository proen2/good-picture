<?php
  session_start();
  $sid=session_id();

  $id = isset($_GET['id']) ? $_GET['id'] : '';

  if (isset($_POST["good_btn"])) {
    if($id){
      include("php/connect.php");
      try{
        $stmt = $conn -> prepare("UPDATE goodimage SET good = good + 1 WHERE id ='{$id}'");
        $stmt->execute();
      }catch(SQLException $e){
        die($e->getMessage());
      }
    }
  }

  if (isset($_POST["bad_btn"])) {
    if($id){
      include("php/connect.php");
      try{
         $stmt = $conn -> prepare("UPDATE goodimage SET bad = bad + 1 WHERE id ='{$id}'");
         $stmt->execute();
      }catch(SQLException $e){
        die($e->getMessage());
      }
    }
  }

  function getTable($id) {
    $r = null;
    if($id){
      include("php/connect.php");
      try{
        $result = $conn->query("SELECT * FROM goodimage WHERE id ='{$id}'");
        $r = $result -> fetch();
      }catch(SQLException $e){
        die($e->getMessage());
      }
    }
    return $r;
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/countUp.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
  <header>
    <div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
    <div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <div class="main_contents">
      <div class="pic_frame">
        <div class="main_contents_pic">
          <img src="php/readImage.php?id=<?php echo $id ?>" alt="画像" />
        </div>
        <!--main_contents_pic-->
      </div>
      <!--pic_frame-->
      <div id="title">
        <?php
          $r = getTable($id);
          echo $r["name"];
        ?>
      </div>
      <!-- #title -->
      <div class="main_content_button">
        <form method="POST" action="">
          <input type="submit" class="good_btn" value="Good" name="good_btn">
          <input type="submit" class="bad_btn" value="Bad" name="bad_btn">
        </form>
      </div>
      <!--main_content_button-->
      <div class="good_bad_num">
        <div class="good_num">
          <img id="good_icon" src="img/icon/good_icon.png" alt="good" />
          <p id="good_count">
            <?php
              echo $r["good"];
            ?>
          </p>
        </div>
        <!--good_num-->
        <div class="bad_num">
          <img id="bad_icon" src="img/icon/bad_icon.png" alt="good" />
          <p id="bad_count">
            <?php
              echo $r["bad"];
            ?>
          </p>
        </div>
        <!--bad_num-->
      </div>
      <!--good_bad_num-->
    </div>
    <!-- main_contennts -->
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
  <script>
    var options = {  
       useEasing: true,
      useGrouping: true,
      separator: ',',
      decimal: '.',
      prefix: '',
      suffix: ''
    };
    var good = new CountUp("good_count", 0, <?php echo $r["good"]; ?>, 0, 1, options);
    var bad = new CountUp("bad_count", 0, <?php echo $r["bad"]; ?>, 0, 1, options);
    good.start();
    bad.start();
  </script>
</body>

</html>
