<?php
	session_start();
	$sid=session_id();

	if($_SESSION[$sid]["sid"]==$sid){
    $user = isset($_GET['user']) ? $_GET['user'] : '';

    if($user){
      include("php/connect.php");
      try{
        $result = $conn->query("SELECT * FROM GP_users WHERE userID = '{$user}'");
        $r = $result -> fetch();

        $_SESSION[$sid]["user"] = $r["userID"];
      }catch(SQLException $e){
        die($e->getMessage());
      }
    }
	}else{
		header("Location: login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>good picture</title>
  <link rel="shortcut icon" href="img/icon/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/reset.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">
  <link type="text/css" rel="stylesheet" href="css/flexslider.css">
  <script src="js/jquery-2.1.4.min.js" charset="utf-8"></script>
  <script src="js/jquery.flexslider-min.js" charset="utf-8"></script>
  <script src="js/script.js" charset="utf-8"></script>
</head>

<body>
	<header>
		<div class="logo">
      <a href="index.php">
        <img src="img/goodpicture.png" alt="">
      </a>
    </div>
		<div class="login_box">
      <?php
        if(isset($_SESSION["name"])){
          $url = '<a class="users_link" href="users.php?user='.$_SESSION["userID"].'">ユーザーページヘ</a>';
          echo "こんにちは".$_SESSION["name"]."さん!!<br>";
          echo $url;
        }else {
          echo '
                <a href="login.php">
                  <button type="button" class="generic_button">Login</button>
                </a>
              ';
        }
      ?>
    </div>
    <!-- login_box -->
    <form class="search_box" method="GET" action="search.php">
      <input type="text" name="keyword" value="" placeholder="キーワードを入力">
      <input type="submit" value="検索">
    </form>
    <!-- search_box -->
  </header>
  <div class="wrapper">
    <div class="main_contents">
			<div class="user_info">
				<?php
					if($r){
	        	echo "こんにちは".$r["name"]."さん<br>";
	        	echo "userID:".$r["userID"]."<br>";
	        	echo "password:".$r["password"]."<br>";
					}
				?>
			</div>
      <!-- .user_info -->
            <a href="upload.php">
			    <button class="generic_button">UPLOAD</button>
            </a>
			<br>
			<?php
				echo '<a href="logout.php"><button class="generic_button">Logout</button></a>';
			?>
		</div>
    <!-- main_contennts -->
  </div>
  <!--wrapper-->
  <footer>
    <div class="footer_content clearfix">
      <ul class="footer_list clearfix">
        <li>
          <a href="index.php">HOME</a>
        </li>
        <li>|</li>
        <li>
          <a href="create.php">ユーザー登録</a>
        </li>
        <li>|</li>
        <li>
          <a href="contact.php">お問い合わせ</a>
        </li>
      </ul>
    </div>
    <!--footer_content-->
  </footer>
</body>

</html>
