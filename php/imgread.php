<?php
  /* あるidの値で特定されるレコード１件を表示する */

  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $r  = null; // idで特定したユーザのレコード（配列）

  if($id){
    include("connect.php");
    try{
    	// SELECT文を実行し，レコード１件の配列を得る。
      $result = $conn->query(
        "SELECT * FROM goodimage WHERE id = {$id}");
      $r = $result -> fetch();
    }catch(SQLException $e){
      die($e->getMessage());
    }
  }
?>
